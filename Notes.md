This file is for taking notes as you go.

2/22: Set up gitlab account and forked repo. installed go/vsc go extensions. Plan is to do thorough code read tomorrow while reading and learning go syntax/functionality. Will sit with the code/reading overnight to complete assignment Wednesday



2/23: Read server code alongside go documents. Commented code to give myself refreshers on what the go does when I reread. Spent lot of time just going through 'gobyexample.com' articles before and during reading to learn go from scratch.

Tried running app. Seems to do nothing when I click send. Inspecting using chrome tools show that health check consistently fails. But even before the healthcheck we can see the socket connection is refused.

Tried running tests to see if this shows up but the test failed because the 'ok' string in the json encoding was capitalized. Simply changing the json encoding tag on the string to be lowercase fixed the test but not the actual app.

Quick inspection of front end code/console shows that backend check is just getting errors and the websocket is still refusing to connect.

Wow lol so I checked the package.json serve script and didn't realize it wss ONLY serving the frontend files on 8080 and not concurrently running the server. On a new terminal I ran the server and then the frontend and now it works...

Sort of works, after small amount of time the health check fails. Browser is mad that origin 127.0.0.1:8080 doesn't match origin localhost:8080. One fix is to make the response origin the same as the request's origin instead of some fixed string.

Replaced origin with network string for now but TODO: need to look into that more

App seems to be working now and the failing test was fixed. Good stopping point for now but I'll put my firts impressions about the other test:

From assignment desc the target issue is about how whatever is being tested is not actually useful: the client send a different shape message or uses the response differently than expected. From my skim of the client code it looks like the message sent over the socket is of the shape { text, uuid } and adding a little log statement to the readPump confirms this.

So in reality we should be testing with a payload byte array corresponding to a JSON object of shape { text, uuid } and making sure that its contents are published correctly. Although I suppose the only thing that would really be checking is that the line:

    message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))

works correctly for stringified JSON objects. 

I'll come back to this tomorrow when I complete the assignment, though I am already pushing 2 hours with how much time I spent learning Go to grok the server code. On reflection it was unnecessary to go as in depth as I did with the server code since the issues I've found so far are quite superficial but I'm still glad I did because I understand Go a little better now and see why it is popular for writing concurrent code.



2/26: Ended up being much busier this week than expected so only just getting around to wrapping this up now. Going to try to timebox myself to 30 more minutes today to finish up.

Last time I identified that we should really be testing that stringified JSON objects get passed along sockets correctly and upon reviewing the frontend socket implementation I've confirmed that Vue native socket does expect JSON in the response data field.

One weird thing is when I was double checking this via log statements in the socket Vue store I found that none of the mutation functions were actually being called on message or socket open. From my (limited) understanding of Vue: we created the socket in the Vue instance with the store so Vue should be wrapping all socket acceeses through the store but I guess that's not actually how it works and the store is not getting utilized since the actual client code is just directly accessing via this.$socket.

Just realized I also never explicitly invoked 'go mod download' but it seems like just running the go code had go download the modules I needed in realtime and cache them somewhere. My go.sum file changed and added an entry for objx again so I'll jsut check it in for now.

Anyways back to the insufficient test: I'm sure that the JSON thing is what we want. I'll just make the message a byte array of a stringified JSON object and confirm that the parsed response can be read as a JSON object.

Got the code working after some debugging issues with marshalling the ClientMessage struct object (properties needed to be capitalized and had to annotate with lowercase json encoding) but I'm mostly out of my timelimit. I'll outline what my plan would have been to complete objective 3 of adding the names to the UI:

1. Add a separate textbox for entering the user's name and bind it to a data variable "username"
2. Lock out the actual message textbar until the "username" varaiable is populated
3. Edit the "sendMessage" function to also send a JSON property "username" that maps to the vue data variable "username"
4. Edit the message display chip to show the message.username field before the text

In terms of the bonus assignments my plan was:

SECURITY: Since we have no real saved data I don't think the security concern is about data so I'm thinking it has something to do with reliability: i.e. a malicious actor can do something that causes a fatal error in the server or causes the server to do something bad to another client. I guess one thing is we never validate the incoming message so a bad actor can create the websocket connection and have the hub publish custom messages that could cause issues with other clients. One fix could be to maybe validate that any message is encodable as a JSON since we know our clients will try to parse it as a JSON.

PRETTIFY: I'm new to Vue so it would've taken a bit of time to learn how to marshall the template containers to add more spacing between messages and move everything to the bottom of the screen with older messages getting pushed up off the top of the screen instead of the send message field being pushed off the bottom. Also should add checks for message size not being empty or too big.
