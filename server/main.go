package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/upchieve/two-am-takeout/server/handlers"
	"gitlab.com/upchieve/two-am-takeout/server/sockets"
)

func main() {
	// set up a context that can be passed to all goroutines
	// with cancel so they can be cleaned up if a sig is received
	var ctx context.Context // built in go object for passing processing state, can be 'nested'
	{
		var cancel context.CancelFunc
		ctx = context.Background()            // generate root context object
		ctx, cancel = context.WithCancel(ctx) // give context a cancel function that will kill process when called
		defer cancel()                        // call cancel when this function (main) ends

		// if we receive a sigint or sigterm, we call cancel,
		// which should cause all goroutines to return
		// then we kill the app
		sigc := make(chan os.Signal, 1) // channels let you pass values between processes
		// sigc has buffer size 1 but even though we ask for 2 types of signals
		// sigc can hold the first signal until go func() below is ready which will take hte signal and kill the main process anyways
		signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM) // Will pass SIGINT/SIGTERM signals from other processes to channel sigc
		// spawn process that awaits sigc channel value
		go func() {
			sig := <-sigc
			cancel() // we only pass sigc SIGINT/SIGTERM so we know the signal means call cancel
			fmt.Println("received a SIGINT or SIGTERM")
			log.Fatal(sig)
		}()
	}
	// start server on this main process
	startServer(ctx)

}

func startServer(ctx context.Context) {
	for {
		select {
		case <-ctx.Done(): // ctx.Done() is a channel that provides value when child contexts are cancelled
			return // end the server process when its children call cancel
		default:
			hub := sockets.NewHub() // create a single hub to handle clients
			go hub.Run()            // start the hub in child process

			r := mux.NewRouter() // router for managing https server handler
			// on /health get request send back {OK: tue} i.e. heartbeat
			r.HandleFunc("/health", handlers.ServeHealth).Methods("Get")

			// on getting /ws request call sockets.ServeWs
			// will put client into hub
			r.HandleFunc("/ws", handlers.ServeSocket(hub))
			r.Use(mux.CORSMethodMiddleware(r)) // standard cors middleware for http req

			srv := &http.Server{
				Handler:      r,
				Addr:         "127.0.0.1:3000",
				WriteTimeout: 15 * time.Second,
				ReadTimeout:  15 * time.Second,
			}

			log.Println("starting server on port 3000")
			log.Fatal(srv.ListenAndServe()) // start server log it's return value as fatal
			// srv.ListenAndServe() is spinning on default only interrupted by
			// a block on the case <-ctx.Done()
		}
	}
}
