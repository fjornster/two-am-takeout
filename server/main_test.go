package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

type ClientMessage struct {
	Text string `json:"text"`
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx) // use test suite context instead of main

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel() // end suite process
}

func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// TODO: Why not check that respBody is the correct object instead of this string thing??
	// make sure health body works matches what the frontend is expecting
	respString := string(respData)
	assert.Contains(suite.T(), respString, "\"ok\":true") // seemsl ike we're happy if repsonse has extra stuff
}

func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// TODO: The functions below should be async to account for server/network repsonse time?
	// checking that text formatted messages go through and can be read correctly
	// Check that input JSON works
	m := ClientMessage{"Hello, Websocket!"}
	b, err := json.Marshal(m)
	assert.Nil(suite.T(), err)
	sender.WriteMessage(websocket.TextMessage, b)

	_, message, err := reader.ReadMessage()
	var respBody ClientMessage
	err = json.Unmarshal(message, &respBody)
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), m, respBody)
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}
