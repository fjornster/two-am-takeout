package sockets

import (
	"bytes"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512 // less than buffer size
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// get socket with these specs
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// Cross-Origin stuff is a pain
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Client is a middleman between the websocket connection and the hub.
// denormalized so clients map to hub and hub has client map
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.

// the single writer/reader is nice because the hub process has no ability to cause race conditions on the socket
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}() // at end of function unregister client from hub and close websocket
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	// TODO: we could put unregister logic into a closehandler function
	// every pong update read deadline to be pongWait seconds from time of pong recieve
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	// default ping handler returns the pong for us
	for {
		_, message, err := c.conn.ReadMessage() // will automatically call ping/pong handlers before reading
		// TODO: Callers should always process the n > 0 bytes returned before considering the error err. Doing so correctly handles I/O errors that happen after reading some bytes and also both of the allowed EOF behaviors.
		if err != nil {
			// handle websocket logic
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			// TODO: could just return err here too
			break // ends function to defer above to handle application logic
		}
		// TODO: check validity of this cleanup function
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		// log.Printf("message: %v", string(message))
		c.hub.broadcast <- message
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close() // doesn't provide hub.unregister? ->
		// I guess only one of read/write needs to since both will die on closure of send ->
		// no jk we need to call unregister here too because if write fails independent of send closure
		// then the write process will end and connection will be closed without removing client from hub ->
		// although connection being ended will cause error in read which will then provide unregister
	}()
	for {
		select {
		// on message added by hub
		// If any case hits an error it returns and kills this function
		case message, ok := <-c.send: // ok is a check on if the channel is closed
			// update deadline to writeWait seconds from now
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub (or maybe someone else) closed the send channel
				// handle websocket closing logic - c.conn.close() will not send close message for us
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return // end function and call defer to handle application logic
			}

			// get a text writer
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				// TODO: should probably return the err so we know what happened
				return // writer utility somehow failed so end
			}
			w.Write(message) // write 'first' element from buffer

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send) // pull subsequent messages form buffer
				// TODO: check if buffer can change during this process (i.e. hub pushes to channel)
			}
			// TODO: we never check message size ourseleves so we could exede and cause an error in conn

			// closing writer flushes content to connection
			if err := w.Close(); err != nil {
				return
			}
		// on every tick send a ping
		case <-ticker.C:
			// update ping deadline
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// ServeWs handles websocket requests from the peer.
func ServeWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
