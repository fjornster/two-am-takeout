package sockets

// gets access to others package exports for free??

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// All of these elements use pointer to reference the client
	// channels await result from somewhere else
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

// NewHub creates a new Hub
// returns a pointer to the hub
func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

// Run starts a Hub process
func (h *Hub) Run() {
	for {
		select {
		// interesting way of defining 'member functions'
		// create channel for args and do something when channel is populated
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client) // remove element from the map
				close(client.send)        // close client.send channel
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				// client send channel is closed so remove the client
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
